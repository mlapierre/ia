# -*- coding: utf-8 -*-
from ...auth_ino import authenticate_client

import scrapy

class InoSpider(scrapy.Spider):
    name = "ino"
    allowed_domains = ["inoreader.com"]
    start_urls = 

    def start_requests(self):
        client = authenticate_client()
        
        urls = [LIKED_URL, RECENTLY_READ_URL, SKIPPED_URL]
        for url in urls:
            r = client.get(url, verify=settings.VERIFY_SSL)
            feed = json.loads(r.text)


            yield scrapy.Request(url=url, callback=self.parse)    

    def parse(self, response):
        pass
