from requests_oauthlib import OAuth2Session
from datetime import datetime
import os
import codecs
import json
import settings
import logging

logger = logging.getLogger(__name__)

def token_saver(token, filename=None):
    if not filename:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        filename = os.path.join(dir_path, 'token.json')
    with codecs.open(filename, 'w', 'utf-8') as fp:
        json.dump(token, fp)

def authorise_new_token():
    oauth = OAuth2Session(settings.CLIENT_ID, redirect_uri=settings.REDIRECT_URI)
    authorization_url, state = oauth.authorization_url(settings.AUTH_URL)
    print('To authorize access please visit: %s' % authorization_url)
    authorization_response = input('Enter the full callback URL: ')

    token = oauth.fetch_token(
            settings.TOKEN_URL,
            authorization_response=authorization_response,
            client_secret=settings.CLIENT_SECRET)
    token_saver(token)
    return token

def token_loader(filename):
    if os.path.isfile(filename):
        with codecs.open(filename, 'r', 'utf-8') as fp:
            return json.load(fp)
    else:
        return authorise_new_token()

def authenticate_client():
    if settings.TOKEN_FILEPATH and os.path.isfile(settings.TOKEN_FILEPATH):
        filename = settings.TOKEN_FILEPATH
    else:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        filename = os.path.join(dir_path, 'token.json')

    token = token_loader(filename)
    expiry = datetime.fromtimestamp(token['expires_at'])

    if datetime.now() > expiry:
        logger.debug("Token expired at: %s", expiry)
        token['expires_in'] = -30
    else:     
        logger.debug("Token expires at: %s", expiry)

    logger.debug(token)    

    extra = {
        'client_id': settings.CLIENT_ID,
        'client_secret': settings.CLIENT_SECRET
    }

    client = OAuth2Session(settings.CLIENT_ID, token=token, auto_refresh_url=settings.TOKEN_URL,
        auto_refresh_kwargs=extra, token_updater=token_saver)
    return client

# Authenticate a new token. If one exists it will be overwritten
def new_token(filename=None):
    if not filename:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        filename = os.path.join(dir_path, 'token.json')

    if os.path.isfile(filename):
        os.remove(filename)
    
    token = authorise_new_token()
    token_saver(token, filename)
    