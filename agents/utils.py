from .auth_ino import authenticate_client, new_token
from data.repository import *
from .fetcher import Fetcher
from datetime import datetime
from pprint import pprint
import settings
import json
import logging

def fetch_and_save(target_url, category):
    logger = logging.getLogger(__name__)
    repo = Repository()
    fetcher = Fetcher()

    logger.info("Fetching %s items", category)

    last_fetched_at = repo.get_last_fetched(target_url)
    if last_fetched_at == None:
        logger.info("First time fetching from: %s", target_url)
    else:
        logger.info("Last fetched at: %s", last_fetched_at  )
    
    url = repo.get_or_add_url(target_url)

    data = fetcher.get_json_from(target_url)
    items = data['items']

    feed = repo.get_feed_by_url(target_url)
    if feed == None:
        feed = Feed(title=data['title'], url=url)
        repo.add(feed)
    
    repo.set_last_fetched(feed, datetime.now())

    # For each item/article, if we haven't already fetched it do so and save it to the DB
    saved = 0
    while True:
        for item in data['items']:
            _, new_item = repo.save_item(item, feed, tags=[category])
            if new_item:
                saved += 1
        logger.info("Saved %d new or updated %s items", saved, category)

        # Stop there if we've already fetched all those items. Otherwise, fetch any more new ones
        if saved == 0 or 'continuation' not in data or len(data['continuation']) == 0:
            logger.info("No more new %s items", category)
            break
        else:
            saved = 0
            logger.info("Fetching more %s items", category)
            data = fetcher.get_json_from(target_url + "?c=" + data['continuation'])
            items += data['items']
            repo.set_last_fetched(feed, datetime.now())        

    return items

def marked_read_unopened(stream_items, recently_read_items):
    # Returns the stream items that are marked as read but weren't viewed. I.e., the stream_items that aren't recently_read_items
    recently_read_ids = list(map(lambda x: x['id'], recently_read_items))
    return list(filter(lambda x: 'user/1005906269/state/com.google/read' in x['categories'] and \
                                 x['id'] not in recently_read_ids, stream_items))

def marked_read_not_skipped(stream_items):
    # Returns the stream items that are marked as read and were not tagged as skipped
    return list(filter(lambda x: "user/1005906269/state/com.google/read" in x['categories'] and \
                                 "user/1005906269/label/skipped" not in x['categories'], stream_items))

def fetch_all():
    logger = logging.getLogger(__name__)

    repo = Repository()

    # Fetch liked articles. Classify as liked
    liked_items = fetch_and_save(settings.LIKED_URL, 'liked')

    # Fetch skipped articles. Classify as skipped
    #skipped_items = fetch_and_save(settings.SKIPPED_URL, 'skipped')

    # Fetch items in Inoreader's 'read' feed. This includes only items that were loaded and read. 
    # E.g., it doesn't include items marked as read by marking all remaining articles in a feed as read.
    recently_read_items = fetch_and_save(settings.RECENTLY_READ_URL, 'read')

    # Collate a list of all feeds/streamIds for each new article picked up from read, liked, and skipped articles
    #streams = set(list(map(lambda x: x['origin']['streamId'], liked_items + skipped_items + recently_read_items)))
    streams = set(list(map(lambda x: x['origin']['streamId'], liked_items + recently_read_items)))

    # Fetch recent items from each stream
    stream_items = []
    fetcher = Fetcher()
    for stream in streams:
        stream_items += fetcher.fetch_stream_once(stream, n=100)

    # Find and tag the 'read' items that were effectively skipped
    # (i.e., the stream items that were marked as read but not included in the Recently Read feed, e.g., by marking all read without opening the item)
    # marked_read_unopened_items = marked_read_unopened(stream_items, recently_read_items)
    # for item in marked_read_unopened_items:
    #     repo.tag(item, tags=['skipped'])
    #     logger.debug("Tagged: skipped")

    # Find and tag the items that were read (or at least loaded)
    # (i.e., filter out unread and skipped items (because Inoreader doesn't let me fetch just those I've read))
    marked_read_not_skipped_items = marked_read_not_skipped(stream_items)
    for item in marked_read_not_skipped_items:
        repo.tag(item, tags=['read'])
        logger.debug("Tagged: read")

    # For each item, fetch the html of the sources and save it too
    # Don't tag the pages because there are tags on the items
    fetch_and_save_item_pages()

def fetch_and_save_item_pages():
    logger = logging.getLogger(__name__)

    repo = Repository()
    url_objs = repo.get_urls_to_save()
    repo.close()
    fetch_and_save_pages(url_objs)

def fetch_and_save_pages(url_objs, tags=None):
    logger.info("%d urls of pages to save", len(url_objs))
    repo = Repository()
    fetcher = Fetcher()

    already_saved = 0
    saved = 0
    url_status = { 'pdf': 0, 'success': 0 }
    tagged = 0
    for url_obj in url_objs:
        if isinstance(url_obj, str):
            url_obj = repo.get_or_add_url(url_obj)
        logger.debug("url: %s", url_obj.url)
        if url_obj.url.endswith(".pdf"):
            url_status['pdf'] += 1
            repo.url_fetched_at(url_obj, datetime.now(), 'pdf')
            continue
        saved_page = repo.get_page_for_url(url_obj)
        if saved_page:
            already_saved += 1
            repo.url_fetched_at(url_obj, datetime.now(), 'success')
            url_status['success'] += 1
        else:
            page, status = fetcher.fetch_page(url_obj.url)
            repo.url_fetched_at(url_obj, datetime.now(), status)
            if status:
                logger.debug("Fetch status: %s", status)
            if status in url_status:
                url_status[status] += 1
            else:
                url_status[status] = 1
            if page:
                saved_page = repo.save_page(url_obj, page=page)
                if saved_page:
                    saved += 1
        if saved_page and tags:
            if repo.tag_page(saved_page, tags):
                tagged += 1
                    
    logger.info("%d urls of pages to save", len(url_objs))
    logger.info("status:")
    status_count = 0
    for status, count in url_status.items():
        logger.info("  %d %s", count, status)
        status_count += count
    logger.info("%d total pages processed", status_count)
    if tagged > 0:
        logger.info("%d pages tagged", tagged)

def new_ino_token():
    new_token(settings.TOKEN_FILEPATH)