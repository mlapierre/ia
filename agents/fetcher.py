from .auth_ino import authenticate_client
from newspaper import Article
import settings
import json
import logging
import requests

logger = logging.getLogger(__name__)

class Fetcher:
    def __init__(self):
        self.client = authenticate_client()
            
    def get_read_items(self):
        return self.get_json_from(settings.RECENTLY_READ_URL)

    def get_json_from(self, url):
        logger.debug("Fetching from: %s", url)

        r = self.client.get(url, verify=settings.VERIFY_SSL)
        return json.loads(r.text)

    def fetch_stream_once(self, stream_id, n=20):
        logger.info("Fetching %s", stream_id)
        data = self.get_json_from(settings.API_BASE + stream_id + "?n=" + str(n))
        return data['items']

    def fetch_stream(self, stream_id, n=20):
        logger.info("Fetching %s", stream_id)
        data = self.get_json_from(settings.API_BASE + stream_id + "?n=" + str(n))
        items = data['items']

        while True:
            if 'continuation' not in data or len(data['continuation']) == 0:
                logger.info("No more new items")
                break
            else:
                logger.info("Fetching more items")
                data = self.get_json_from(settings.API_BASE + stream_id + "?c=" + data['continuation'] + "&n=" + str(n))
                items += data['items']

        return items

    def fetch_page(self, url):
        logger.debug("Fetching from: %s", url)

        try:
            r = requests.get(url, verify=settings.VERIFY_SSL)
            if r.headers['Content-Type'] == 'application/pdf':
                logger.debug('The url content is a pdf. Skipping')
                return None, 'pdf'
            if not (r.headers['Content-Type'].startswith('text') 
                 or r.headers['Content-Type'].endswith('html')):
                logger.debug('The url content is not text. Skipping')
                return None, 'non-text'
            if r.text:
                article = Article(url, fetch_images=False)
                article.download(input_html=r.text)
                article.parse()
                if len(article.text) > 0:
                    return article, 'success'
                else:
                    logger.debug('The article content is empty. Skipping')
            else:
                logger.error("The url did not return any content")
            return None, 'no-content'
        except requests.exceptions.RequestException as e:
            logger.error("There was an error with the request: ", e)
            return None, 'error'