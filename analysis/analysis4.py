
# coding: utf-8

# In[14]:
if __name__ == '__main__':

    import nltk
    import string
    from nltk.stem.porter import PorterStemmer

    nltk.download('punkt')

    stemmer = PorterStemmer()

    def stem_tokens(tokens, stemmer):
        stemmed = []
        for item in tokens:
            stemmed.append(stemmer.stem(item))
        return stemmed

    def tokenize(text):
        no_punc_trans = str.maketrans('', '', string.punctuation)
        tokens = nltk.word_tokenize(text.translate(no_punc_trans))
        stems = stem_tokens(tokens, stemmer)
        return stems


    # In[15]:


    import os
    from sklearn.feature_extraction.text import TfidfVectorizer
    from sklearn.svm import LinearSVC
    from sklearn.pipeline import Pipeline
    from sklearn.model_selection import GridSearchCV
    from sklearn.ensemble import AdaBoostClassifier
    from sklearn.datasets import load_files
    from sklearn.linear_model import SGDClassifier
    from sklearn.model_selection import train_test_split
    from sklearn import metrics


    # In[16]:


    data_path = os.path.join(os.getcwd(), '..', 'data', 'to_analyse')
    dataset = load_files(data_path, shuffle=False)
    print("n_samples: %d" % len(dataset.data))
    print(dataset.target)
    print(dataset.target_names)


    # In[17]:


    dataset.data = dataset.data[:5000]
    dataset.target = dataset.target[:5000]
    print("n_samples: %d" % len(dataset.data))


    # In[18]:


    dataset.data[0][:500]


    # In[19]:


    # split the dataset in training and test set using a specific seed for reproducibility
    docs_train, docs_test, y_train, y_test = train_test_split(
        dataset.data, dataset.target, test_size=0.25, random_state=80)


    # In[36]:


    # Vectorise to extract features and filter out tokens that are too rare or too frequent
    pipeline = Pipeline([
        ('vect', TfidfVectorizer(tokenizer=tokenize, min_df=3, max_df=0.95, ngram_range=(1, 3))),
        ('clf', AdaBoostClassifier()),
    ])


    # In[42]:


    from sklearn.model_selection import StratifiedKFold

    n_estimators = [100, 250, 500, 1000, 10000]
    learning_rate = [0.0001, 0.001, 0.01, 0.1, 1]
    #n_estimators = [100]
    #learning_rate = [0.0001]
    param_grid = dict(clf__learning_rate=learning_rate, clf__n_estimators=n_estimators)
    kfold = StratifiedKFold(n_splits=10, shuffle=True, random_state=7)
    grid_search = GridSearchCV(pipeline, param_grid, scoring="neg_log_loss", n_jobs=-1, cv=kfold, verbose=10)
    #grid_search = GridSearchCV(pipeline, param_grid, scoring="neg_log_loss", cv=kfold, verbose=10)
    grid_result = grid_search.fit(docs_train, y_train)

    # summarize results
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))


    # In[ ]:


    # plot results
    import numpy
    # Don't try to use Xwindows, pyplot
    import matplotlib
    matplotlib.use('Agg')
    from matplotlib import pyplot
    scores = numpy.array(means).reshape(len(learning_rate), len(n_estimators))
    for i, value in enumerate(learning_rate):
        pyplot.plot(n_estimators, scores[i], label='learning_rate: ' + str(value))
    pyplot.legend()
    pyplot.xlabel('n_estimators')
    pyplot.ylabel('Log Loss')
    pyplot.savefig('n_estimators_vs_learning_rate.png')
