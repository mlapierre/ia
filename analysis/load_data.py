# Retrieve a dataset containing hits (liked articles) and misses (read articles that were not liked)

from os.path import abspath, pardir, join, dirname
import sys
sys.path.append(join(dirname(abspath(__file__)), pardir))

from data.repository import Repository
from agents.fetcher import Fetcher
from newspaper import Article
from datetime import datetime
import numpy as np
import logging
import settings
import os
import codecs

logger = logging.getLogger(__name__)

def load_articles_tagged(tag):
    repo = Repository()
    items = repo.items_with_tag(tag)
    pages = repo.pages_with_tag(tag)

    logger.info('%d items in the db tagged \'%s\'', len(items), tag)
    logger.info('%d pages in the db tagged \'%s\'', len(pages), tag)

    # Merge items and pages. Page content overrides item content because it should be
    # the full article as text, instead of the feed summary as html

    # Start with pages so we can next find items without pages
    items_dict = {}    
    for item in pages:
        items_dict[item.url.url] = {
            'title': item.title,
            'content': item.article_text
        }

    # Fetch the full article text content of any item doesn't have a page
    fetcher = Fetcher()
    for item in items:
        if item.url.url not in items_dict:
            if len(item.content) > 0:
                article = Article(item.url.url, fetch_images=False)
                article.download(input_html=item.content)
                article.parse()
            if len(item.content) > 0 and len(article.text) > 0:
                items_dict[item.url.url] = {
                    'title': article.title,
                    'content': article.text
                }
            else:
                logger.info("Item missing text content. URL: %s", item.url.url)
                saved_page = repo.get_page_for_url(item.url)
                if not saved_page or not saved_page.article_text or len(saved_page.article_text) == 0:
                    url_history = repo.url_fetch_history(item.url, limit=1)
                    if url_history and len(url_history) > 0:
                        logger.info("Already attempted to fetch item source. Skipping...")
                        continue
                    logger.info("Fetching item source")
                    page, status = fetcher.fetch_page(item.url.url)
                    repo.url_fetched_at(item.url, datetime.now(), status)
                    logger.debug("Fetch status: %s", status)
                    if status != 'success':
                        continue
                    saved_page = repo.save_page(item.url, page=page)
                    repo.tag_page(saved_page, tag)
                if not saved_page:
                    logger.warn("Could not fetch item source content")
                    continue
                if not saved_page.article_text or len(saved_page.article_text) == 0:
                    logger.warn("Item source content was also empty.")
                    continue
                items_dict[item.url.url] = {
                    'title': saved_page.title,
                    'content': saved_page.article_text
                }

    logger.info('%d unique items total tagged \'%s\'', len(items_dict), tag)
    return items_dict

def load_data():
    # Load the data into a Bunch to be used by sklearn
    target = []
    target_names = ['hit', 'miss']
    shuffle = False

    hits = load_articles_tagged('liked')
    data = list(map(lambda item: item['content'], hits.values()))
    target.extend(len(data) * [0])

    read = load_articles_tagged('read')
    misses = []
    # The liked items will also be tagged read, so skip them
    for key in read.keys():
        if key not in hits:
            misses.append(read[key]['content'])

    logger.info('%d read items total', len(read))
    logger.info('%d misses', len(misses))

    data += misses
    target.extend(len(misses) * [1])

    # convert to array for fancy indexing
    target = np.array(target)

    if shuffle:
        random_state = check_random_state(random_state)
        random_state.shuffle(indices)
        target = target[indices]

    return {
        'data': data,
        'target_names': target_names,
        'target': target
    }

def export_data_to(data, path):
    for name in data.target_names:
        folder = os.path.join(path, name)
        if not os.path.exists(folder):
            os.makedirs(folder)

    for i in range(len(data.data)):
        filename = os.path.join(path, data.target_names[data.target[i]], str(i) + '.txt')
        with codecs.open(filename, "w", "utf-8") as fp:
            if data.data[i]and len(data.data[i]) > 0:
                fp.write(data.data[i])
