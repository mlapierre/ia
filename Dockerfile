FROM python:3.6

RUN apt-get update && apt-get install -y \
    libboost-all-dev \
 && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir pipenv && \
    pipenv install -r requirements.txt

ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY ./ ./

RUN chmod +x ./scripts/*.py

EXPOSE 8888

CMD [ "./scripts/fetch_all.py" ]