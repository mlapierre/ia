-- Titles of liked items
select i.title from Items as i
  join ItemTags as it on i.id = it.item_id
  join Tags as t on t.id = it.tag_id
 where t.tag = 'liked';

-- Counts of liked items
select count(*) from Items as i
  join ItemTags as it on i.id = it.item_id
  join Tags as t on t.id = it.tag_id
 where t.tag = 'liked';
select count(*) from Pages as p
  join PageTags as pt on p.id = pt.page_id
  join Tags as t on t.id = pt.tag_id
 where t.tag = 'liked';

-- All items and pages, including pages without items, and items without pages.
select p.title as ptitle, i.title as ititle, u.url from Pages as p  
right join URLs as u on p.url_id = u.id
left join Items as i on i.url_id = u.id

-- All liked items and pages
select p.title as ptitle, i.title as ititle, u.url, tp.tag as ptag, ti.tag as itag from Pages as p  
right join URLs as u on p.url_id = u.id
left join Items as i on i.url_id = u.id
full outer join PageTags as pt on p.id = pt.page_id
full outer join ItemTags as it on i.id = it.item_id
full outer join Tags as ti on ti.id = it.tag_id
full outer join Tags as tp on tp.id = pt.tag_id
where ti.tag = 'liked' OR tp.tag = 'liked'

-- All read items and pages that were not liked
select p.title as ptitle, i.title as ititle, u.url from Pages as p
right join URLs as u on p.url_id = u.id
left join Items as i on i.url_id = u.id
full outer join PageTags as pt on p.id = pt.page_id
full outer join ItemTags as it on i.id = it.item_id
full outer join Tags as ti on ti.id = it.tag_id
full outer join Tags as tp on tp.id = pt.tag_id
where (ti.tag = 'read' or tp.tag = 'read') and u.id not in
  (select u.id from Pages as p  
  right join URLs as u on p.url_id = u.id
  left join Items as i on i.url_id = u.id
  full outer join PageTags as pt on p.id = pt.page_id
  full outer join ItemTags as it on i.id = it.item_id
  full outer join Tags as ti on ti.id = it.tag_id
  full outer join Tags as tp on tp.id = pt.tag_id
  where ti.tag = 'liked' OR tp.tag = 'liked')

-- All skipped items and pages
select p.title as ptitle, i.title as ititle, u.url from Pages as p
right join URLs as u on p.url_id = u.id
left join Items as i on i.url_id = u.id
full outer join PageTags as pt on p.id = pt.page_id
full outer join ItemTags as it on i.id = it.item_id
full outer join Tags as ti on ti.id = it.tag_id
full outer join Tags as tp on tp.id = pt.tag_id
where (ti.tag = 'skipped' or tp.tag = 'skipped') and u.id is not null