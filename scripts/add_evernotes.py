#!/usr/local/bin/python

from os.path import abspath, pardir, join, dirname
import sys
sys.path.append(join(dirname(abspath(__file__)), pardir))

import os, glob, codecs
from pprint import pprint
from repository.repository import Repository
from datetime import datetime
from fetcher.fetcher import Fetcher
from fetcher.utils import fetch_and_save
import logging
import settings

logger = logging.getLogger(__name__)

repo = Repository()
fetcher = Fetcher()

dir_path = os.path.dirname(os.path.realpath(__file__))
source_path = os.path.join(dir_path, "data", "processed")

urls = []

for filename in glob.glob(os.path.join(os.path.normpath(source_path), '*.txt')):
    with codecs.open(filename, "r", "utf-8") as fp:
        data = fp.read()
        meta = data.split('\n\n')
        sourcelines = list(filter(lambda x: x.startswith('Source:'), meta[1].split('\n')))
        if len(sourcelines) <= 0:
            continue
        url = sourcelines[0].split('Source:')[1]
        if not url.endswith(".pdf"):
            urls.append(url)
print(len(urls))
# for url in urls:
#     page = fetcher.fetch_page(url)
#     if page:
#         repo.save_page(page, tags=['read', 'liked'])

repo.close()
