#!/usr/local/bin/python

from os.path import abspath, pardir, join, dirname
import sys
sys.path.append(join(dirname(abspath(__file__)), pardir))

from analysis.load_data import *

if len(sys.argv) < 2:
    raise Exception("Please provide the path to export data to")

data = load_data()

print("Exporting data to " + sys.argv[1])
export_data_to(data, sys.argv[1])