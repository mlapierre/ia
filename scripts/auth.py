#!/usr/local/bin/python

from os.path import abspath, pardir, join, dirname
import sys
sys.path.append(join(dirname(abspath(__file__)), pardir))

from agents.utils import new_ino_token

new_ino_token()