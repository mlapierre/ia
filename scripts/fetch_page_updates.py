#!/usr/local/bin/python

from os.path import abspath, pardir, join, dirname
import sys
sys.path.append(join(dirname(abspath(__file__)), pardir))

from agents.utils import fetch_page_updates

fetch_page_updates()