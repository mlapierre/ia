#!/usr/local/bin/python

from os.path import abspath, pardir, join, dirname
import sys
sys.path.append(join(dirname(abspath(__file__)), pardir))

import os
import codecs
import json
import logging
from agents.utils import fetch_and_save_pages

logger = logging.getLogger(__name__)

dir_path = os.path.dirname(os.path.realpath(__file__))
hits_file = os.path.join(dir_path, '..', 'data', 'hits.json')
with codecs.open(hits_file, 'r', 'utf-8') as jsonfile:
    urls = json.load(jsonfile)

logger.info("%d urls stored", len(urls))
urls = list(set(urls))
fetch_and_save_pages(urls, tags=['liked'])

logger.info("Done.")