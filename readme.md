A personal project to manage the information firehose

To start with, it will collect every article I mark read in all the RSS feeds I subscribe to, along with tags indicating if I liked them.

The articles form a dynamic dataset used to train a model to predict articles I'll like.

TODO
----

* Prepare data for analysis
* Use Scrapy to fetch content
* Manually add articles (currently saved in Pocket)
 - Use pocket's api to fetch: https://getpocket.com/developer/docs/v3/retrieve
* Ranked predictions instead of binary classification
* Grouped predictions (e.g., ranked by topic)
* Handle tweets


Wistlist
--------

* Crawl links from interesting articles to:
 * find original sources where available
 * find similar interesting content
* Crawl interesting *topics* (i.e., generate topics from interesting articles and use them as search keywords, then crawl the results)
* Search for links *to* interesting articles to find commentary, including contrasting perspectives
* Perform other types of analysis and compare results with a public API (e.g., https://cloud.google.com/natural-language/)
