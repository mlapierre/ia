from os.path import abspath, pardir, join, dirname
import sys
sys.path.append(join(dirname(abspath(__file__)), pardir))

from agents.utils import marked_read_unopened, marked_read_not_skipped

def test_marked_read_unopened():
    read_category = 'user/1005906269/state/com.google/read'
    stream_items = [
        {'id': 0, 'categories': ['read but not skipped', read_category]},
        {'id': 2, 'categories': ['skipped but not read']},
        {'id': 4, 'categories': ['skipped', read_category]}
    ]
    recently_read_items = [
        {'id': 0, 'categories': ['read but not skipped', read_category]},
        {'id': 1, 'categories': [read_category]},
        {'id': 3, 'categories': ['should not be here']}
    ]

    marked_read_unopened_items = marked_read_unopened(stream_items, recently_read_items)

    assert stream_items[0] not in marked_read_unopened_items
    assert stream_items[1] not in marked_read_unopened_items
    assert stream_items[2] in marked_read_unopened_items
    assert recently_read_items[0] not in marked_read_unopened_items
    assert recently_read_items[1] not in marked_read_unopened_items
    assert recently_read_items[2] not in marked_read_unopened_items

def test_marked_read_not_skipped():
    skipped_category = 'user/1005906269/label/skipped'
    read_category = 'user/1005906269/state/com.google/read'
    stream_items = [
        {'id': 0, 'categories': ['read but not skipped', read_category]},
        {'id': 1, 'categories': ['skipped but not read', skipped_category]},
        {'id': 2, 'categories': ['skipped and read (should not exist)', read_category, skipped_category]}
    ]

    marked_read_not_skipped_items = marked_read_not_skipped(stream_items)

    assert stream_items[0] in marked_read_not_skipped_items
    assert stream_items[1] not in marked_read_not_skipped_items
    assert stream_items[2] not in marked_read_not_skipped_items
