from os.path import join, dirname
from os import environ, getenv
from dotenv import load_dotenv
from logging.config import fileConfig
import logging

fileConfig('logging_config.ini')

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

API_BASE = 'https://www.inoreader.com/reader/api/0/stream/contents/'
RECENTLY_READ_URL = API_BASE + 'user/-/state/com.google/recently-read'
SKIPPED_URL = API_BASE + 'user/-/label/skipped'
LIKED_URL = API_BASE + 'user/-/state/com.google/like'

CLIENT_ID = environ['INOREADER_CLIENT_ID']
CLIENT_SECRET = environ['INOREADER_SECRET']
REDIRECT_URI = 'https://mlapierre.github.io/oauth2/callback'
AUTH_URL = 'https://www.inoreader.com/oauth2/auth'
TOKEN_URL = 'https://www.inoreader.com/oauth2/token'
TEST_URL = 'https://www.inoreader.com/reader/api/0/user-info'

TOKEN_FILEPATH = getenv('TOKEN_FILEPATH', join(dirname(__file__), 'agents', 'token.json'))

DB_USER = environ['DB_USER']
DB_PASS = environ['DB_PASS']
DB_HOST = environ['DB_HOST']
DB_NAME = environ['DB_NAME']

VERIFY_SSL=True
DEBUG=False