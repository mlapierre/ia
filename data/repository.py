import logging
import settings
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.declarative import as_declarative, declared_attr
from sqlalchemy.orm import relationship, backref, sessionmaker
from sqlalchemy.sql import func
from sqlalchemy import Column, Integer, String, TIMESTAMP, ForeignKey, create_engine, Table
from datetime import datetime

logger = logging.getLogger(__name__)

@as_declarative()
class Base(object):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower() + "s"
    
    id = Column(Integer, primary_key=True)

# https://bitbucket.org/zzzeek/sqlalchemy/wiki/UsageRecipes/UniqueObject
def _unique(session, cls, hashfunc, queryfunc, constructor, arg, kw):
    cache = getattr(session, '_unique_cache', None)
    if cache is None:
        session._unique_cache = cache = {}

    key = (cls, hashfunc(*arg, **kw))
    if key in cache:
        return cache[key]
    else:
        with session.no_autoflush:
            q = session.query(cls)
            q = queryfunc(q, *arg, **kw)
            obj = q.first()
            if not obj:
                obj = constructor(*arg, **kw)
                session.add(obj)
        cache[key] = obj
        return obj        
    
class UniqueMixin(object):
    @classmethod
    def unique_hash(cls, *arg, **kw):
        raise NotImplementedError()

    @classmethod
    def unique_filter(cls, query, *arg, **kw):
        raise NotImplementedError()

    @classmethod
    def as_unique(cls, session, *arg, **kw):
        return _unique(
                    session,
                    cls,
                    cls.unique_hash,
                    cls.unique_filter,
                    cls,
                    arg, kw
               )

class URL(UniqueMixin, Base):
    url = Column(String)

    url_fetch_history = relationship("URLFetchHistory", back_populates="url")

    def __repr__(self):
        return "<URL(id='%s', url='%s')>" % (self.id, self.url)

    @classmethod
    def unique_hash(cls, url):
        return url

    @classmethod
    def unique_filter(cls, query, url):
        return query.filter(URL.url == url)    

class Feed(Base):
    title = Column(String)
    stream = Column(String)
    url_id = Column(Integer, ForeignKey('urls.id'))

    url = relationship("URL")
    feed_fetch_history = relationship("FeedFetchHistory", back_populates="feed")

    def __repr__(self):
        return "<Feed(title='%s')>" % (self.title)

class Stream(Base):
    description = Column(String)
    stream = Column(String)

    def __repr__(self):
        return "<Stream(description='%s', stream='%s')>" % (self.description, self.stream)

class Item(Base):
    title = Column(String)
    content = Column(String)
    created_at = Column(TIMESTAMP)
    url_id = Column(Integer, ForeignKey('urls.id'))
    feed_id = Column(Integer, ForeignKey('feeds.id'))

    url = relationship("URL")
    feed = relationship("Feed")
    page = relationship("Page", back_populates="item", uselist=False)
    it = relationship("Tag", secondary=lambda: itemtags_table)
    tags = association_proxy('it', 'tag')

    def __repr__(self):
        return "<Item(title='%s')>" % (self.title)

class Page(Base):
    title = Column(String)
    html = Column(String)
    article_text = Column(String)
    created_at = Column(TIMESTAMP)
    url_id = Column(Integer, ForeignKey('urls.id'))
    item_id = Column(Integer, ForeignKey('items.id'))

    url = relationship("URL")
    item = relationship("Item", back_populates="page")
    pt = relationship("Tag", secondary=lambda: pagetags_table)
    tags = association_proxy('pt', 'tag')

    def __repr__(self):
        return "<Page(title='%s')>" % (self.title)

class FeedFetchHistory(Base):
    fetch_time = Column(TIMESTAMP)
    status = Column(String)
    feed_id = Column(Integer, ForeignKey('feeds.id'))

    feed = relationship("Feed", back_populates="feed_fetch_history")

    def __repr__(self):
        return "<FeedFetchHistory(fetch_time='%s', status='%s')>" % (self.fetch_time, self.status)

class URLFetchHistory(Base):
    fetch_time = Column(TIMESTAMP)
    status = Column(String)
    url_id = Column(Integer, ForeignKey('urls.id'))

    url = relationship("URL", back_populates="url_fetch_history")

    def __repr__(self):
        return "<URLFetchHistory(fetch_time='%s', status='%s')>" % (self.fetch_time, self.status)

class Tag(UniqueMixin, Base):
    tag = Column(String)

    def __init__(self, tag):
        self.tag = tag

    def __repr__(self):
        return "<Tag(id='%s', tag='%s')>" % (self.id, self.tag)

    @classmethod
    def unique_hash(cls, tag):
        return tag

    @classmethod
    def unique_filter(cls, query, tag):
        return query.filter(Tag.tag==tag)

itemtags_table = Table('itemtags', Base.metadata,
    Column('item_id', Integer, ForeignKey("items.id"),
           primary_key=True),
    Column('tag_id', Integer, ForeignKey("tags.id"),
           primary_key=True)
)

pagetags_table = Table('pagetags', Base.metadata,
    Column('page_id', Integer, ForeignKey("pages.id"),
           primary_key=True),
    Column('tag_id', Integer, ForeignKey("tags.id"),
           primary_key=True)
)

class Repository():
    def __init__(self):
        dburl = "postgresql://{}:{}@{}/{}".format(settings.DB_USER, settings.DB_PASS, settings.DB_HOST, settings.DB_NAME)
        engine = create_engine(dburl, echo=settings.DEBUG)
        Session = sessionmaker(bind=engine)
        self.session = Session()
        Base.metadata.create_all(engine)        
    
    def add(self, instance):
        self.session.add(instance)
        self.session.commit()
    
    def commit(self):
        self.session.commit()

    def close(self):
        self.session.close()

    def query(self, *entities, **kwargs):
        return self.session.query(*entities, **kwargs)

    def get_feed_by_url(self, url):
        return self.session.query(Feed).join(URL).filter(URL.url==url).one_or_none()

    def get_or_add_feed_by_url(self, url):
        feed = self.get_feed_by_url(url)
        if not feed:
            url = self.get_or_add_url(url)
            feed = Feed(url=url)
            self.add(feed)
        return feed
        
    def get_last_fetched(self, url):
        return self.session.query(func.max(FeedFetchHistory.fetch_time)).join(Feed).join(URL).filter(URL.url==url).scalar()
    
    def set_last_fetched(self, feed, timestamp):
        fft = FeedFetchHistory(feed=feed, fetch_time=timestamp)
        self.session.add(fft)
        self.session.commit()

    def get_or_add_url(self, target_url):
        url = self.session.query(URL).filter_by(url=target_url).one_or_none()
        if url != None:
            logger.debug("Already saved url: %s", target_url)
        else:
            logger.info("Saving url: %s", target_url)
            url = URL(url=target_url)
            self.add(url)
        return url

    # Save an item to the db, including url and tags.
    # First detect if url, item, or tag have already been saved.
    def save_item(self, item, feed, tags=None):
        link = item['canonical'][0]['href']
        url = self.get_or_add_url(link)
        feed = self.session.query(Feed).filter_by(url=feed.url).one()
        
        saved_item = self.session.query(Item).filter_by(url_id=url.id).one_or_none()
        saved = False
        if saved_item:
            logger.debug("Already saved item with title: %s", item['title'])
        else:
            logger.info("Saving item with title: %s", item['title'])
            saved_item = Item(title=item['title'], 
                              content=item['summary']['content'],
                              created_at=datetime.now(), 
                              feed=feed, 
                              url=url)
            self.add(saved_item)
            logger.debug("Saved item with id: %s", saved_item.id)
            saved = True

        for tag in tags:
            if tag not in saved_item.tags:
                unique_tag = Tag.as_unique(self.session, tag=tag)
                saved_item.it.append(unique_tag)
        logger.debug("Tagged: %s", saved_item.tags)
        self.session.commit()
        
        return saved_item, saved

    # Save the new tags that aren't already saved for the item
    # TODO combine with save_item
    def tag(self, item, tags):
        link = item['canonical'][0]['href']
        url = self.get_or_add_url(link)
        saved_item = self.session.query(Item).join(URL).filter(URL.url==link).one_or_none()
        feed = self.get_or_add_feed_by_url(item['origin']['streamId'])

        if saved_item != None:
            logger.debug("Already saved item with title: %s", item['title'])
        else:
            logger.info("Saving item with title: %s", item['title'])
            saved_item = Item(title=item['title'], 
                              content=item['summary']['content'],
                              created_at=datetime.now(), 
                              feed=feed, 
                              url=url)
            self.add(saved_item)
            logger.debug("Saved item with id: %s", saved_item.id)
        
        for tag in tags:
            if tag not in saved_item.tags:
                unique_tag = Tag.as_unique(self.session, tag=tag)
                saved_item.it.append(unique_tag)
        logger.debug("Tagged: %s", saved_item.tags)
        self.session.commit()

    def items_with_tag(self, tag):
        return self.session.query(Item).filter(Item.tags.contains(tag)).all()

    def pages_with_tag(self, tag):
        return self.session.query(Page).filter(Page.tags.contains(tag)).all()

    # Returns URLs of items that don't have an entry in the Pages table
    # Exclude URLs that have already been fetched
    def get_urls_to_save(self):
        return (self.session.query(URL).join(Item).outerjoin(Page).outerjoin(URLFetchHistory)
                            .filter(Page.url_id==None)
                            .filter(URLFetchHistory.url_id==None)
                            .all())

    def get_page_for_url(self, url):
        return self.session.query(Page).join(URL).filter(URL.url==url.url).one_or_none()

    def save_page(self, url, page=None, tags=None):
        saved_page = self.get_page_for_url(url)
        if saved_page:
            logger.debug("Already saved page with url: %s", url.url)
            return saved_page

        item = self.session.query(Item).join(URL).filter(URL.url==url.url).one_or_none()

        saved_page = Page(title=page.title,
                          html=page.html,
                          article_text=page.text,
                          url=url,
                          item=item,
                          created_at=datetime.now())
        logger.debug("Saving page: %s", saved_page.url.url)

        self.add(saved_page)
        return saved_page

    def tag_page(self, page, tags):
        tagged = False
        if isinstance(tags, str):
            tags = [tags]
        for tag in tags:
            if tag not in page.tags:
                unique_tag = Tag.as_unique(self.session, tag=tag)
                page.pt.append(unique_tag)
                tagged = True
        if tagged:
            self.session.commit()
        logger.debug("Tagged: %s", page.tags)
        return tagged

    def tag_url(self, url, tags):
        tagged = False
        if isinstance(tags, str):
            tags = [tags]
        url_rec = URL.as_unique(self.session, url=url)
        page, _ = self.get_or_create(Page, {"url":url_rec})
        for tag in tags:
            if tag not in page.tags:
                unique_tag = Tag.as_unique(self.session, tag=tag)
                page.pt.append(unique_tag)
                tagged = True
        if tagged:
            self.session.commit()
        logger.debug("Tagged: %s", page.tags)
        return tagged

    def get_or_create(self, model, defaults=None, **kwargs):
        instance = self.session.query(model).filter_by(**kwargs).first()
        if instance:
            return instance, False
        else:
            params = dict((k, v) for k, v in kwargs.iteritems() if not isinstance(v, ClauseElement))
            params.update(defaults or {})
            instance = model(**params)
            self.session.add(instance)
            return instance, True

    def url_fetched_at(self, url, fetch_time, status):
        event = URLFetchHistory(
            url=url,
            fetch_time=fetch_time,
            status=status)
        self.add(event)

    def url_fetch_history(self, url, limit=None):
        return (self.session.query(URLFetchHistory)
            .filter_by(url=url)
            .order_by(URLFetchHistory.fetch_time)
            .limit(limit)
            .all())