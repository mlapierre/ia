#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER fetcher;
    CREATE DATABASE fetcher;
    GRANT ALL PRIVILEGES ON DATABASE fetcher TO fetcher;
EOSQL

psql -v ON_ERROR_STOP=1 -U fetcher -d fetcher <<-EOSQL
    CREATE TABLE urls (
        id serial primary key,
        url text UNIQUE
    );
    CREATE TABLE feeds (
        id serial primary key, 
        stream text,
        title text, 
        url_id integer REFERENCES urls,
        UNIQUE (title, url_id)
    );
    CREATE TABLE streams (
        id serial primary key,
        description text,
        stream text
    );
    CREATE TABLE items (
        id serial primary key,
        feed_id integer REFERENCES feeds,
        title text,
        url_id integer REFERENCES urls,
        content text,
        created_at timestamp
    );
    CREATE TABLE pages (
        id serial primary key,
        item_id integer REFERENCES items,
        title text,
        url_id integer REFERENCES urls,
        html text,
        article_text text,
        created_at timestamp
    );
    CREATE TABLE fetchStatus (
        id serial primary key,
        status text
    );
    CREATE TABLE feedFetchHistories (
        id serial primary key,
        feed_id integer REFERENCES feeds,
        fetch_time timestamp,
        status integer REFERENCES fetchStatus
    );
    CREATE TABLE urlFetchHistories (
        id serial primary key,
        url_id integer REFERENCES urls,
        fetch_time timestamp,
        status integer REFERENCES fetchStatus
    );
    CREATE TABLE tags (
        id serial primary key,
        tag text UNIQUE
    );
    CREATE TABLE itemTags (
        item_id serial REFERENCES items,
        tag_id serial REFERENCES tags,
        UNIQUE (item_id, tag_id)
    );
    CREATE TABLE pageTags (
        page_id serial REFERENCES pages,
        tag_id serial REFERENCES tags,
        UNIQUE (page_id, tag_id)
    );
EOSQL